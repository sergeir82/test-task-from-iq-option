var DropdownApp = angular.module('DropdownApp', []);

//Использование компонента, без ng-repeat
DropdownApp.controller('ExampleController1', function ($scope) {});

//Использование компонента, без ng-repeat и без групп
DropdownApp.controller('ExampleController2', function ($scope) {});

//Использование компонента с фиксированной высотой выпадающего списка
DropdownApp.controller('ExampleController3', function ($scope) {});

//Использование компонента c ng-repeat
DropdownApp.controller('ExampleController4', function ($scope) {
    $scope.cars = [
        {
            title: 'Седаны',
            list: [
                {id: 1, title: 'Mazda 6'},
                {id: 2, title: 'Bmw 328i'},
                {id: 3, title: 'Mercedes C'},
                {id: 4, title: 'Audi A4'}
            ]
        },
        {
            title: 'Кроссоверы',
            list: [
                {id: 5, title: 'Mazda CX-5'},
                {id: 6, title: 'Bmw X5'},
                {id: 7, title: 'Mercedes ML'},
                {id: 8, title: 'Audi Q7'}
            ]
        }
    ];

    // Функция добавляет модели машин Model* с группой Group*
    $scope.changeList = function() {
        var num = Math.floor((Math.random()*6)+1),
            name = 'Group' + num,
            models = [];

        for (var i = 0; i < num; i++) {
            models.push({id: i, title: 'Model' + i });
        }

        $scope.cars.push({
            title: name,
            list: models
        });
    }

    //Эту переменную мы привязываем к атрибуту ng-model компонента
    $scope.selectedCar = '' ;

    $scope.changeTimes = 0;

    $scope.onChangeEvent = function(){
        $scope.changeTimes = $scope.changeTimes + 1;
        $scope.$apply();
    }

});

// Директива элемента dropdown
DropdownApp.directive('dropdown', function () {
    return {
        restrict: "E",
        replace: true,
        transclude: true,
        require: '?ngModel',    // Атрибут ngModel у нас необязательный
        template: '<div class="dropdown">' +
                    '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">' +
                        '<span>Dropdown</span><span class="caret"></span>' +
                    '</button>' +
                    '<ul ng-style="style" class="dropdown-menu" aria-labelledby="dropdownMenu1" role="menu" ng-transclude></ul>' +
                '</div>',
        link: function (scope, element, attrs, ngModel) {
            var oldVal;

            // Определяем функцию в родительской области видимости переменной
            // для изменения переменной указанной нами в атрибуте ng-model,
            // т.к. мы используем изолированную область видимости
            scope.$parent.setValue = function(val){

                var onChange = scope.onChange();

                if(ngModel){
                    ngModel.$setViewValue(val);
                }
                scope.$parent.$apply();

                if(angular.isFunction(onChange) && oldVal != val){
                    oldVal = val;
                    onChange();
                }
            }

            // Задаем стили выпадающего списка
            scope.style = {
                width : scope.width,
                'max-height': scope.maxHeight ? scope.maxHeight.toString().replace(/^(\d+)$/, '$1px') : null,
                overflow: 'auto'
            }
        },
        scope: {
            onChange: '&',
            width: '@',
            maxHeight: '@'
        }
    }
});

// Директива элемента itemGroup
DropdownApp.directive('itemGroup', function () {
    return {
        restrict: "E",
        replace: true,
        transclude: true,
        template: '<li class="dropdown-menu-group" ng-click="$event.stopPropagation();">' +
                '<strong >{{title}}</strong>' +
            '</li>',
        scope: {
            'title': '@'
        },
        compile: function (templateElement, templateAttrs, transclude) {
            return {
                pre: function ($scope, element, attrs, controller) {

                },
                post: function ($scope, element, attrs, controller) {
                    // Помещаем всё содержимое элемента itemGroup после элемента шаблона
                    transclude($scope.$parent, function (clone) {
                        element.after(clone);
                    })
                }
            }

        }
    }
})

// Директива элемента item
DropdownApp.directive('item', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: '<a href="#" role="menuitem" ng-transclude></a>',
        link: function (scope, element, attrs, controller, transclude) {

            transclude(scope.$parent, function (clone) {
                element.wrap('<li>');
                element.html(clone);

            })

            // Событие выбора элемента списка
            element.on('click', function(){
                scope.setValue(attrs.value);
            })
        }
    }
})
